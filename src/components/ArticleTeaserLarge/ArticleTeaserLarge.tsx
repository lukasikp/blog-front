import React, { Component } from "react";

import { Link } from "react-router-dom";
import { apiUrl } from "../../constans/url";

import "./ArticleTeaserLarge.style.scss";

interface Props {
  image: string;
  title: string;
  lead: string;
  url: string;
  category: string;
  color: string;
  date: string;
  _id: string;
}

export default class ArticleTeaserLarge extends React.Component<Props, {}> {
  constructor(props: Props) {
    super(props);
  }
  public render(): JSX.Element {
    return (
      <div className="articleTeaserLarge">
        <div className="articleTeaserLarge__content">
          <Link
            className="articleTeaserLarge__link"
            to={{
              pathname: `/artykuly/${this.props.url}`,
              state: { _id: this.props._id }
            }}
          >
            <p className="articleTeaserLarge__title">{this.props.title}</p>
            <div className="articleTeaserLarge__info">
              <div
                className="articleTeaserLarge__info-color"
                style={{ backgroundColor: this.props.color || "#f878b3" }}
              />
              <span className="articleTeaserLarge__separate">
                {this.props.date.substring(0, 10)}
              </span>
              <span className="articleTeaserLarge__span">
                {this.props.category}
              </span>
            </div>
            <p className="articleTeaserLarge__text">{this.props.lead}</p>
          </Link>
        </div>
        <Link
          className="articleTeaserLarge__image"
          to={{
            pathname: `/artykuly/${this.props.url}`,
            state: { _id: this.props._id }
          }}
        >
          <div className="image">
            <img src={`${apiUrl}/${this.props.image}`} />
          </div>
        </Link>
      </div>
    );
  }
}
