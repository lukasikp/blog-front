import React, { Component } from "react";

import { connect } from "react-redux";
import { getArticles } from "../../ducks/articles/articles";

import "./Pagination.style.scss";

const categoryReg: RegExp = /category=.*([&]?)$/;
const parseQuery = (url: string): any =>
  Array.from(new URLSearchParams(url.split("?")[1]).entries()).reduce(
    (q, [k, v]) => Object.assign(q, { [k]: v }),
    {}
  );

interface Props {
  count: number;
  total: number;
  next: string | null;
  previous: string | null;
  offset: number;
  getArticles: any;
}
interface State {
  limit: number;
}

class Pagination extends React.Component<Props, State> {
  state = {
    limit: 4
  };

  handleClick = (offset: string, category: string) => {
    window.scrollTo(0, 0);
    this.props.getArticles(offset, category);
  };
  getLinkToParseCategory = (next: string | null, prev: string | null) => {
    let url: string;
    url = next !== null ? this.getCategoryId(next) : "";
    url = prev !== null ? this.getCategoryId(prev) : url;
    return url;
  };

  getCategoryId = (url: string): string => parseQuery(url).category;

  public render(): JSX.Element {
    const { count, total, next, previous, offset, getArticles } = this.props;
    const { limit } = this.state;
    const maxOffset: number = Math.ceil(total / limit) * limit;
    const category = this.getLinkToParseCategory(next, previous);
    return (
      <div className="pagination">
        {(offset - maxOffset) / limit < 4 && offset - 4 * limit >= 0 && (
          <p
            className="pagination__item"
            onClick={() => this.handleClick(`${offset - 4 * limit}`, category)}
          >
            {offset / limit - 3}
          </p>
        )}
        {(offset - maxOffset) / limit < 3 && offset - 3 * limit >= 0 && (
          <p
            className="pagination__item"
            onClick={() => this.handleClick(`${offset - 3 * limit}`, category)}
          >
            {offset / limit - 2}
          </p>
        )}
        {(offset - maxOffset) / limit < 4 && offset - 2 * limit >= 0 && (
          <p
            className="pagination__item"
            onClick={() => this.handleClick(`${offset - 2 * limit}`, category)}
          >
            {offset / limit - 1}
          </p>
        )}
        {(offset - maxOffset) / limit < 5 && offset - limit >= 0 && (
          <p
            className="pagination__item"
            onClick={() => this.handleClick(`${offset - limit}`, category)}
          >
            {offset / limit}
          </p>
        )}
        <p className="pagination__item pagination__item--active">
          {offset / limit + 1}
        </p>
        {offset + 2 * limit <= maxOffset && (
          <p
            className="pagination__item"
            onClick={() => this.handleClick(`${offset + 1 * limit}`, category)}
          >
            {offset / limit + 2}
          </p>
        )}
        {offset + limit * 3 <= maxOffset && (
          <p
            className="pagination__item"
            onClick={() => this.handleClick(`${offset + 2 * limit}`, category)}
          >
            {offset / limit + 3}
          </p>
        )}
        {offset + 4 * limit <= maxOffset && (
          <p
            className="pagination__item"
            onClick={() => this.handleClick(`${offset + 3 * limit}`, category)}
          >
            {offset / limit + 4}
          </p>
        )}
        {offset + 5 * limit <= maxOffset && (
          <p
            className="pagination__item"
            onClick={() => this.handleClick(`${offset + 4 * limit}`, category)}
          >
            {offset / limit + 5}
          </p>
        )}
      </div>
    );
  }
}

const mapStateToProps = ({
  articles: {
    articles: { count, total, next, previous, offset }
  }
}: any) => ({ count, total, next, previous, offset });

export default connect(
  mapStateToProps,
  { getArticles }
)(Pagination);
