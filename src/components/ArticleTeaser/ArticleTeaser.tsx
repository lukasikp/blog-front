import React, { Component } from "react";

import { Link } from "react-router-dom";
import { apiUrl } from "../../constans/url";

import "./ArticleTeaser.style.scss";

interface Props {
  image: string;
  title: string;
  lead: string;
  category: string;
  color: string;
  date: string;
  url: string;
  _id: string;
}

export default class ArticleTeaser extends React.Component<Props, {}> {
  constructor(props: Props) {
    super(props);
  }
  public render(): JSX.Element {
    return (
      <div className="articleTeaser">
        <div className="articleTeaser__image-container">
          <Link
            className="articleTeaser__image"
            to={{
              pathname: `/artykuly/${this.props.url}`,
              state: { _id: this.props._id }
            }}
          >
            <div className="image">
              <img src={`${apiUrl}/${this.props.image}`} />
            </div>
          </Link>
          <div className="articleTeaser__category-tekst">
            <div
              className="articleTeaser__category-color"
              style={{ backgroundColor: this.props.color }}
            />
            <p className="articleTeaser__category">{this.props.category}</p>
          </div>
        </div>
        <div className="articleTeaser__content">
          <Link
            className="articleTeaser__link"
            to={{
              pathname: `/artykuly/${this.props.url}`,
              state: { _id: this.props._id }
            }}
          >
            <p className="articleTeaser__title">{this.props.title}</p>
            <p className="articleTeaser__date">
              {this.props.date.substring(0, 10)}
            </p>
            <p className="articleTeaser__text">{this.props.lead}</p>
          </Link>
        </div>
      </div>
    );
  }
}
