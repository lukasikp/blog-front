import React, { Component } from "react";
import { Carousel, CarouselItem } from "reactstrap";
import { connect } from "react-redux";

import ArticleTeaserLarge from "../ArticleTeaserLarge/ArticleTeaserLarge";
import { MainSliderControl } from "./MainSliderControl";

interface CarouselState {
  readonly activeIndex: number;
}

class MainSlider extends Component<any, any> {
  public animating: Boolean = false;

  readonly state: CarouselState = {
    activeIndex: 0
  };

  numberSlides = 4;

  onExiting = () => {
    this.animating = true;
  };

  onExited = () => {
    this.animating = false;
  };

  next = () => {
    if (this.animating) return;
    const nextIndex =
      this.state.activeIndex === this.numberSlides - 1
        ? 0
        : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  };

  previous = () => {
    if (this.animating) return;
    const nextIndex =
      this.state.activeIndex === 0
        ? this.numberSlides - 1
        : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  };

  goToIndex = (newIndex: number) => {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  };

  renderSlides = (articles: any) => {
    return articles.map((item: any, v: number) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item._id}
        >
          <ArticleTeaserLarge
            image={item.articleImage}
            title={item.title}
            url={item.url}
            lead={item.lead}
            category={item.category ? item.category.name : ""}
            color={item.category.color}
            date={item.publishedAt}
            _id={item._id}
          />
        </CarouselItem>
      );
    });
  };

  render() {
    const { activeIndex } = this.state;
    const data = this.props.articles.result;
    const articles = data !== undefined ? data.slice(0, this.numberSlides) : [];
    const slides = this.renderSlides(articles);
    if (articles.length > 0 && articles.length < this.numberSlides) {
      this.numberSlides = articles.length;
    }

    return (
      <Carousel
        activeIndex={activeIndex}
        next={this.next}
        previous={this.previous}
      >
        {slides}
        <MainSliderControl direction="prev" onClickHandler={this.previous} />
        <MainSliderControl direction="next" onClickHandler={this.next} />
      </Carousel>
    );
  }
}

const mapStateToProps = (state: any) => ({
  articles: state.articles.articles
});

export default connect(
  mapStateToProps,
  {}
)(MainSlider);
