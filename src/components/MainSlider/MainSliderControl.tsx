import React from "react";
import "./MainSliderControl.style.scss";

interface SliderControlProps {
  direction: "next" | "prev";
  onClickHandler: any;
}

export const MainSliderControl = (props: SliderControlProps) => {
  const controlStyle = {
    position: "absolute",
    bottom: 0,
    right: 0,
    backgroundColor: "#f878b3"
  };
  return (
    <div
      className={`main-slider-control main-slider-control--${props.direction}`}
      onClick={props.onClickHandler}
    >
      {props.direction === "next" ? ">>" : "<<"}
    </div>
  );
};
