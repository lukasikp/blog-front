import React from "react";
import { Link } from "react-router-dom";

import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem
} from "reactstrap";

import logo from "../../assets/images/logo.png";
import "./Navbar.scss";

export default class NavbarDropdown extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      isOpen: false
    };
  }
  handleOpen = () => {
    this.setState({ isOpen: true });
  };

  handleClose = () => {
    this.setState({ isOpen: false });
  };

  render() {
    return (
      <UncontrolledDropdown
        nav
        inNavbar
        onMouseOver={this.handleOpen}
        onMouseLeave={this.handleClose}
        isOpen={this.state.isOpen}
      >
        <DropdownToggle className="navbar__link" caret>
          <Link className="navbar__link" to={`/sztuka`}>
            Options
          </Link>
        </DropdownToggle>
        <DropdownMenu right>
          <DropdownItem>Option 1</DropdownItem>
          <DropdownItem>Option 2</DropdownItem>
          <DropdownItem divider />
          <DropdownItem>Reset</DropdownItem>
        </DropdownMenu>
      </UncontrolledDropdown>
    );
  }
}
