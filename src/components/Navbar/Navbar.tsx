import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";

import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Dropdown
} from "reactstrap";

import logo from "../../assets/images/logo.png";
import "./Navbar.scss";
import NavbarDropdown from "./NavbarDropdown";
import { getCategories } from "../../ducks/categories/categories";

class NavbarComponent extends React.Component<any, any> {
  constructor(props: any) {
    super(props);
    this.state = {
      isOpen: false,
      isOpenDropdown: false,
      fixedNav: false
    };
  }
  componentDidMount() {
    this.props.getCategories();
    window.addEventListener("scroll", this.handleScroll);
  }
  toggle = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  };

  handleOpen = () => {
    this.setState({ isOpenDropdown: true });
  };

  handleClose = () => {
    this.setState({ isOpenDropdown: false });
  };

  handleScroll = () => {
    const top = document.documentElement.scrollTop || document.body.scrollTop;
    top >= 50
      ? this.setState({ fixedNav: true })
      : this.setState({ fixedNav: false });
  };

  getMainCategory = () => {
    return this.props.categories.categories.filter(
      (item: any) => item.relations.main_category === true
    );
  };

  render() {
    const mainCategories = this.getMainCategory();
    return (
      <div>
        <Navbar
          expand="md"
          className={`navbar ${this.state.fixedNav && "navbar--fixed"}`}
        >
          <Link
            to={{
              pathname: `/`,
              state: { _id: "", name: "" }
            }}
          >
            <img src={logo} className="navbar__logo" />
          </Link>
          <NavbarToggler onClick={this.toggle} />
          <Collapse
            className="navbar__collapse"
            isOpen={this.state.isOpen}
            navbar
          >
            <Nav className="ml-auto navbar__list" navbar>
              {mainCategories.map(
                (category: { name: String; _id: string; color: string }) => (
                  <NavItem key={`nav${category.name}`}>
                    <Link
                      className="navbar__link"
                      to={{
                        pathname: `/${category.name}`,
                        state: {
                          _id: category._id,
                          name: category.name,
                          color: category.color
                        }
                      }}
                    >
                      {category.name}
                    </Link>
                  </NavItem>
                )
              )}
              {/* <NavItem>
                <Link className="navbar__link" to="/o-nas">
                  O nas
                </Link>
              </NavItem> */}
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  categories: state.categories
});

export default connect(
  mapStateToProps,
  { getCategories }
)(NavbarComponent);
