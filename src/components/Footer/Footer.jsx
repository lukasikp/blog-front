import React from "react";

import { Link } from "react-router-dom";
import facebook from "../../assets/images/facebook.svg";
import twitter from "../../assets/images/twitter.svg";
import instagram from "../../assets/images/instagram.svg";

import "./Footer.style.scss";

class Footer extends React.Component {
  render() {
    const date = new Date();
    return (
      <footer className="footer">
        <div className="footer__icons">
          <a href="https://www.facebook.com/Kultura-i-trendy-2065058300241881/">
            <img src={facebook} className="footer__icon" alt="" />
          </a>
          <a href="https://twitter.com/kulturaitrendy/">
            <img src={twitter} className="footer__icon" alt="" />
          </a>
          <a href="https://www.instagram.com/kulturaitrendy/">
            <img src={instagram} className="footer__icon" alt="" />
          </a>
        </div>
        <div className="footer__links">
          <Link
            className="footer__link"
            to={{
              pathname: `/kontakt`
            }}
          >
            Kontakt
          </Link>
          <Link
            className="footer__link"
            to={{
              pathname: `/o-nas`
            }}
          >
            O kultura i trendy
          </Link>
        </div>
        <p className="footer__text">
          Copyrights © {date.getFullYear()} kulturaitrendy.pl. Wszelkie prawa
          zastrzeżone.{" "}
        </p>
      </footer>
    );
  }
}

export default Footer;
