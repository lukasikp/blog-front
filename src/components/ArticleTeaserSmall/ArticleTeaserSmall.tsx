import React, { Component } from "react";

import { Link } from "react-router-dom";
import { apiUrl } from "../../constans/url";
import "./ArticleTeaserSmall.style.scss";

interface Props {
  image: string;
  title: string;
  url: string;
  lead: string;
  category: string;
  color: string;
  date: string;
  _id: string;
}

export default class ArticleTeaserSmall extends React.Component<Props, {}> {
  constructor(props: Props) {
    super(props);
  }
  public render(): JSX.Element {
    return (
      <div className="articleTeaserSmall">
        <Link
          className="articleTeaserSmall__image"
          to={{
            pathname: `/artykuly/${this.props.url}`,
            state: { _id: this.props._id }
          }}
        >
          <div className="image">
            <img src={`${apiUrl}/${this.props.image}`} />
          </div>
        </Link>
        <p className="articleTeaserSmall__right-align">
          <span className="articleTeaserSmall__date">
            {this.props.date.substring(0, 10)}
          </span>
          <span className="articleTeaserSmall__category">
            {this.props.category}
          </span>
        </p>
        <Link
          className="articleTeaserSmall__link"
          to={{
            pathname: `/artykuly/${this.props.url}`,
            state: { _id: this.props._id }
          }}
        >
          <p className="articleTeaserSmall__title">{this.props.title}</p>
        </Link>
      </div>
    );
  }
}
