import React, { Component } from "react";

import { connect } from "react-redux";

import "./HomeArticleList.style.scss";
import ArticleTeaser from "../ArticleTeaser/ArticleTeaser";
import ArticleTeaserSmall from "../ArticleTeaserSmall/ArticleTeaserSmall";
import Pagination from "../Pagination/Pagination";

interface Props {
  articles: any;
}

interface State {
  width: number;
}

class HomeArticleList extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      width: window.innerWidth
    };
  }

  componentWillMount() {
    window.addEventListener("resize", this.handleWindowSizeChange);
  }

  componentWillUnmount() {
    window.removeEventListener("resize", this.handleWindowSizeChange);
  }

  handleWindowSizeChange = () => {
    this.setState({ width: window.innerWidth });
  };

  public render(): JSX.Element {
    const articles = this.props.articles.articles.result;
    const { count, total } = this.props.articles.articles;

    const { width } = this.state;
    const isMobile = width <= 600;
    if (isMobile) {
      return (
        <div className="home-article-list">
          <div className="home-article-list__col home-article-list__col--thin">
            {articles &&
              articles.map((article: any) => (
                <ArticleTeaserSmall
                  key={article._id}
                  image={article.articleImage}
                  title={article.title}
                  lead={article.lead}
                  category={article.category.name}
                  color={article.category.color}
                  date={article.publishedAt}
                  url={article.url}
                  _id={article._id}
                />
              ))}
          </div>
          {total > count && <Pagination />}
        </div>
      );
    } else {
      const oddArticles =
        articles &&
        articles.filter((el: any, index: number) => index % 2 !== 0);
      const evenArticles =
        articles &&
        articles.filter((el: any, index: number) => index % 2 === 0);
      return (
        <div className="home-article-list">
          <div className="home-article-list__col home-article-list__col--wide">
            {evenArticles &&
              evenArticles.map((article: any) => (
                <ArticleTeaser
                  key={article._id}
                  image={article.articleImage}
                  title={article.title}
                  lead={article.lead}
                  category={article.category.name}
                  color={article.category.color}
                  date={article.publishedAt}
                  url={article.url}
                  _id={article._id}
                />
              ))}
          </div>
          <div className="home-article-list__col home-article-list__col--thin">
            {oddArticles &&
              oddArticles.map((article: any) => (
                <ArticleTeaserSmall
                  key={article._id}
                  image={article.articleImage}
                  title={article.title}
                  lead={article.lead}
                  category={article.category.name}
                  color={article.category.color}
                  date={article.publishedAt}
                  url={article.url}
                  _id={article._id}
                />
              ))}
          </div>
          {total > count && <Pagination />}
        </div>
      );
    }
  }
}

const mapStateToProps = (state: any) => ({
  articles: state.articles
});

export default connect(
  mapStateToProps,
  {}
)(HomeArticleList);
