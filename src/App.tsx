import React, { Component } from "react";
import logo from "./logo.svg";
import "./App.scss";
import NavbarComponent from "./components/Navbar/Navbar";
import Footer from "./components/Footer/Footer";
import { BrowserRouter as Router, Route, Link, Switch } from "react-router-dom";

import Home from "./pages/Home";
import ArticleList from "./pages/ArticleList";
import Article from "./pages/Article";
import AboutUs from "./pages/AboutUs";
import Contact from "./pages/Contact";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router>
          <div>
            <NavbarComponent />
            <div style={{ marginTop: "60px" }}>
              <p>git commit on master</p>

              <Switch>
                <Route exact path="/" component={Home} />
                <Route path="/o-nas" exact component={AboutUs} />
                <Route path="/kontakt" exact component={Contact} />
                <Route path="/artykuly/:id" component={Article} />
                <Route path="/:id" component={ArticleList} />
              </Switch>
            </div>
            <Footer />
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
