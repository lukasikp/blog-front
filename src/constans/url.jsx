export const apiUrl = "http://trendnakulture.pl";
export const categoriesPath = "/categories/";
export const articlesPath = "/articles/";

//offset - default limit - 20
// category - name
export const articlesQueries = (offset = "", category = "") => {
  return `?offset=${offset}&category=${category}`;
};
