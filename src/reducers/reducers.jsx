import { combineReducers } from "redux";
import articles from "../ducks/articles/articles";
import categories from "../ducks/categories/categories";

export default combineReducers({
  articles,
  categories
});
