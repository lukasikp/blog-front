import React, { Component } from "react";
import { connect } from "react-redux";
import { getArticles } from "../ducks/articles/articles";

import MainSlider from "../components/MainSlider/MainSlider";
import HomeArticleList from "../components/HomeArticleList/HomeArticleList";

class Home extends Component<any, any> {
  componentDidMount() {
    this.props.getArticles();
  }
  render() {
    const { previous } = this.props.articles;
    return (
      <div>
        {previous === null && <MainSlider />}
        <HomeArticleList />
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  articles: state.articles.articles
});

export default connect(
  mapStateToProps,
  { getArticles }
)(Home);
