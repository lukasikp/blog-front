import React, { Component } from "react";

import { connect } from "react-redux";
import { getArticles } from "../ducks/articles/articles";

import HomeArticleList from "../components/HomeArticleList/HomeArticleList";

interface ArticleProps {
  location: {
    state: { _id: string; name: string; color: string };
  };
}

interface StateProps {
  article: any;
  loading: boolean;
}

interface DispatchProps {
  getArticles: (offset: string, category: string) => void;
}

type Props = ArticleProps & StateProps & DispatchProps;

class ArticleList extends Component<Props> {
  componentDidMount() {
    this.props.getArticles("0", this.props.location.state._id);
  }
  componentWillReceiveProps(nextProps: any) {
    if (this.props.location.state._id !== nextProps.location.state._id) {
      this.props.getArticles("0", nextProps.location.state._id);
    }
  }
  render() {
    const { color } = this.props.location.state;
    return (
      <div>
        <div className="article-list__header-container">
          <div
            className="article-list__header-color"
            style={{ backgroundColor: color || "#000" }}
          />
          <p className="article-list__header">
            {this.props.location.state.name}
          </p>
        </div>
        <HomeArticleList />
      </div>
    );
  }
}

const mapStateToProps = (state: any) => ({
  articles: state.articles
});

export default connect(
  mapStateToProps,
  { getArticles }
)(ArticleList);
