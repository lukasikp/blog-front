import React, { Component } from "react";

import { connect } from "react-redux";
import {
  ContentState,
  EditorState,
  convertToRaw,
  convertFromRaw
} from "draft-js";
// import draftToHtml from "draftjs-to-html";
import { stateToHTML } from "draft-js-export-html";

import { getArticle } from "../ducks/articles/articles";
import { apiUrl } from "../constans/url";

interface ArticleProps {
  location: {
    state: { _id: string };
  };
}

interface StateProps {
  article: any;
  loading: boolean;
}

interface DispatchProps {
  getArticle: (_id: string) => void;
}

type Props = ArticleProps & StateProps & DispatchProps;

class Article extends Component<Props> {
  state = { editorState: EditorState.createEmpty() };
  componentDidMount() {
    const { _id } = this.props.location.state;
    this.props.getArticle(_id);
  }

  renderArticle = () => {
    const { article } = this.props;

    const content = convertFromRaw(JSON.parse(article.content));

    return (
      <div className="article">
        <div className="article__top">
          <div className="article__column">
            <h1 className="article__title">{article.title}</h1>
            <h3 className="article__lead">{article.lead}</h3>
          </div>
          <img
            className="article__first-img"
            src={`${apiUrl}/${article.articleImage}`}
            alt=""
          />
        </div>
        <div dangerouslySetInnerHTML={{ __html: stateToHTML(content) }} />
      </div>
    );
  };

  render() {
    const { article } = this.props;
    return <div>{article !== undefined && this.renderArticle()}</div>;
  }
}

const mapStateToProps = (state: any) => ({
  article: state.articles.article.result,
  loading: state.articles.loading
});

export default connect(
  mapStateToProps,
  { getArticle }
)(Article);
