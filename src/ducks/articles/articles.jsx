import axios from "axios";
import { apiUrl, articlesPath, articlesQueries } from "../../constans/url";

const FETCH_ARTICLES = "blog-front/articles/FETCH_ARTICLES";
const RECEIVE_ARTICLES = "blog-front/articles/RECEIVE_ARTICLES";
const FETCH_ARTICLE = "blog-front/articles/FETCH_ARTICLE";
const RECEIVE_ARTICLE = "blog-front/articles/RECEIVE_ARTICLE";

const initialState = {
  loaded: false,
  articles: {},
  article: {}
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case FETCH_ARTICLES:
      return {
        ...state,
        loading: true
      };
    case RECEIVE_ARTICLES:
      return {
        ...state,
        loading: false,
        articles: action.payload
      };
    case FETCH_ARTICLE:
      return {
        ...state,
        loading: true
      };
    case RECEIVE_ARTICLE:
      return {
        ...state,
        loading: false,
        article: action.payload
      };
    default:
      return state;
  }
}

const fetchArticles = () => {
  return {
    type: FETCH_ARTICLES
  };
};
const receiveArticles = data => {
  return {
    type: RECEIVE_ARTICLES,
    payload: data
  };
};

const fetchArticle = () => {
  return {
    type: FETCH_ARTICLE
  };
};
const receiveArticle = data => {
  return {
    type: RECEIVE_ARTICLE,
    payload: data
  };
};

export const getArticles = (offset = "", categories = "") => {
  const queries = articlesQueries(offset, categories);
  return dispatch => {
    dispatch(fetchArticles);
    axios
      .get(`${apiUrl}${articlesPath}${queries}`)
      .then(function(response) {
        dispatch(receiveArticles(response.data));
      })
      .catch(function(error) {
        console.log(error);
      });
  };
};

export const getArticle = id => {
  return dispatch => {
    dispatch(fetchArticle);
    axios
      .get(`${apiUrl}${articlesPath}${id}`)
      .then(function(response) {
        dispatch(receiveArticle(response.data));
      })
      .catch(function(error) {
        console.log(error);
      });
  };
};
