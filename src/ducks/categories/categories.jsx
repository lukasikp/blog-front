import axios from "axios";
import { apiUrl, categoriesPath } from "../../constans/url";

const FETCH_CATEGORIES = "blog-front/articles/FETCH_CATEGORIES";
const RECEIVE_CATEGORIES = "blog-front/articles/RECEIVE_CATEGORIES";

const initialState = {
  loaded: false,
  categories: []
};

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case FETCH_CATEGORIES:
      return {
        ...state,
        loaded: false
      };
    case RECEIVE_CATEGORIES:
      return {
        ...state,
        loaded: true,
        categories: action.payload
      };
    default:
      return state;
  }
}

const fetchCategories = () => {
  return {
    type: FETCH_CATEGORIES
  };
};
const receiveCategories = data => {
  return {
    type: RECEIVE_CATEGORIES,
    payload: data
  };
};

export const getCategories = () => {
  return dispatch => {
    dispatch(fetchCategories);
    axios
      .get(`${apiUrl}${categoriesPath}`)
      .then(function(response) {
        dispatch(receiveCategories(response.data.result));
      })
      .catch(function(error) {
        console.log(error);
      });
  };
};
